<?php
/**
 * @file
 * Implementation of the regexp based referer condition.
 */

/**
 * Expose referers as a context condition.
 *
 * based on context_condition_path.
 */
class referer_context_condition_regex extends context_condition {

  /**
   * Omit condition values. We will provide a custom input form for our conditions.
   */
  function condition_values() {
    return array();
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    unset($form['#options']);

    $form['#type'] = 'textarea';
    $form['#default_value'] = implode("\n", $this->fetch_from_context($context, 'values'));
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    $parsed = array();
    $items = explode("\n", $values);
    if (!empty($items)) {
      foreach ($items as $v) {
        $v = trim($v);
        if (!empty($v)) {
          $parsed[$v] = $v;
        }
      }
    }
    return $parsed;
  }

  /**
   * Execute.
   */
  function execute() {
    if ($this->condition_used()) {
      $referers = $_SESSION['referers'];

      foreach ($this->get_contexts() as $context) {
        $regexps = $this->fetch_from_context($context, 'values');

        // Trigger context if any of the regexps matches any of the referers.
        foreach ($regexps as $r) {
          foreach ($referers as $p) {
            if (preg_match($r, $p)) {
              $this->condition_met($context);
            }
          }
        }

      }
    }
  }

}
