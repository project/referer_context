==== What it does ====

This module allows you to trigger contexts based on the referer(s) of the
current session.

Example applications:
* Display larger facebook connect/like buttons if the user entered your site
  by a facebook link.
* Customize your landing-page based on the ad-banner was followed.
* ...

==== How it works ====

This module gathers all external referer URLs and saves them in the user's
session. It implements a context condition that fires if one of the referer
URLs matches a regular expression.

==== Usage ====

* Enable the module.
* Go to /admin/structure/context and edit or add a context
* Add a new condition "Referer"
* Put regular expressions (separated by line breaks) in the text-area
* The condition is met whenever one of the regular expressions matches one
  of the referer URLs.
